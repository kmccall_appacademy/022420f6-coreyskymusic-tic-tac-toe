class Board

  attr_accessor :grid, :marks

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
    @marks = [:X, :O]

  end
  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end




  def winner
    @grid.each do |array|
      if array.all?{|sym| sym == :X}
        return :X
      elsif array.all?{|sym| sym == :O}
        return :O
      end
    end

    @grid.transpose.each do |array|
      if array.all?{|sym| sym == :X}
        return :X
      elsif array.all?{|sym| sym == :O}
        return :O
      end
    end

    return :X if diagonal?(@grid, :X) == true
    return :X if diagonal?(reverser(@grid), :X) == true
    return :O if diagonal?(@grid, :O) == true
    return :O if diagonal?(reverser(@grid), :O) == true

  end

  def reverser(grid)
    mock_grid = []
    grid.dup.each do |array|
      mock_grid << array.dup.reverse
    end
    mock_grid
  end

  def diagonal?(grid, sym)
    number_of_rows = grid.size
    counter = 0
    check_array = []
    grid.each do |array|
      check_array << array[counter]
      counter += 1
    end
    check_array.all?{|sym_check| sym_check == sym}
  end

  def over?
    return true if winner != nil
    if @grid.flatten.any?{|space| space ==  nil}
      false
    else
      true
    end

  end



end
