class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name

  end


  def get_move
    available_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        if @board[pos].nil?
          available_moves << pos
        end
      end
    end

    available_moves.each do |move|
      return move if wins?(move)
    end

    available_moves.shuffle[1]

  end

  def wins?(move)
    @board[move] = mark
    result = @board.winner
    @board[move] = nil
    return true if result == mark
    false
  end




  def display(board)
    @board = board
  end


  def valid_move?(pos)
    empty?(pos)
  end




end
