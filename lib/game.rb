require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor  :player_one, :player_two, :current_player, :board


  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    player_one.mark = :X
    player_two.mark = :O
    @board = Board.new
    @counter = 0
  end


  def play_turn
    puts "#{@current_player.name} It is your turn."
    @board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
    @current_player.display(board)

  end

  def switch_players!
    @counter += 1
    if @counter.odd?
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end


  def play
    @current_player.display(board)
    until @board.over?
      play_turn


    end
    puts "Gave Over"

    if @board.winner == @player_one.mark
      puts "#{@player_one.name} Wins!"
    elsif @board.winner == @player_two.mark
      puts "#{@player_two.name} Wins!"
    else
      "Tie Game."
    end

  end
end


if $PROGRAM_NAME == __FILE__
  print "Enter your name: "
  name = gets.strip
  human1 = HumanPlayer.new(name)
  print "Player Two, enter your name: "
  second_name = gets.strip
  human2 = HumanPlayer.new(second_name)
  new_game = Game.new(human1, human2)
  new_game.play
end
